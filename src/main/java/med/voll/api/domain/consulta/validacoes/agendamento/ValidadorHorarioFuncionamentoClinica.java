package med.voll.api.domain.consulta.validacoes.agendamento;

import med.voll.api.domain.ValidacaoException;
import med.voll.api.domain.consulta.DadosAgendamentoConsulta;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;

@Component
public class ValidadorHorarioFuncionamentoClinica implements ValidadorAgendamentoDeConsulta {
    @Override
    public void validar(DadosAgendamentoConsulta dados) {

        var dataAgendamento = dados.data();
        var domingo = dataAgendamento.getDayOfWeek().equals(DayOfWeek.SUNDAY);
        var foraDoHorarioDeFuncionamento = dataAgendamento.getHour() < 7 || dataAgendamento.getHour() > 18;

        if (domingo || foraDoHorarioDeFuncionamento) {
            throw new ValidacaoException("Consulta fora do horário de funcionamento da clínica");
        }
    }
}



CREATE SEQUENCE IF NOT EXISTS public.medicos_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE  public.medicos_id_seq
    OWNER TO postgres;

--- Table: public.medicos

 -- DROP TABLE IF EXISTS public.medicos;

 CREATE TABLE IF NOT EXISTS public.medicos
 (
     id bigint NOT NULL DEFAULT nextval('medicos_id_seq'::regclass),
     crm character varying(255) COLLATE pg_catalog."default",
     email character varying(255) COLLATE pg_catalog."default",
     bairro character varying(255) COLLATE pg_catalog."default",
     cep character varying(255) COLLATE pg_catalog."default",
     cidade character varying(255) COLLATE pg_catalog."default",
     complemento character varying(255) COLLATE pg_catalog."default",
     logradouro character varying(255) COLLATE pg_catalog."default",
     numero character varying(255) COLLATE pg_catalog."default",
     uf character varying(255) COLLATE pg_catalog."default",
     especialidade character varying(255) COLLATE pg_catalog."default",
     nome character varying(255) COLLATE pg_catalog."default",
     CONSTRAINT medicos_pkey PRIMARY KEY (id)
 )

 TABLESPACE pg_default;

 ALTER TABLE IF EXISTS public.medicos
     OWNER to postgres;